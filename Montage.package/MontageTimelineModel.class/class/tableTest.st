as yet unclassified
tableTest
	UiTableView new
		addColumn: 'Duration'
		for: ((UiItemViewColumnSpec newFrom: {#text -> #duration})
				textConverter: [:integer | integer asString asHtmlText]);
		
		addColumn: 'Transform'
		for: ((UiItemViewColumnSpec newFrom: {#text -> #transform})
				textConverter: [:string | string asHtmlText]);
		
		addColumn: 'Thumbnail'
		for: ((UiItemViewColumnSpec newFrom: {#text -> #yourself})
				textConverter: [:node | node thumbnail]);
		 model: MontageTimelineModel new;
		 setHAlignmentForColumn: 2 to: #right;
		 setHAlignmentForColumn: 3 to: #center;
		 topLeft: 50 @ 50;
		 extent: 600 @ 300;
		 addVerticalHeader;
		 openInWorld