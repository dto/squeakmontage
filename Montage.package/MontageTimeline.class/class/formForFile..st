as yet unclassified
formForFile: file	

(self forms) at: file ifAbsent: [Forms at: file put: (PNGReadWriter formFromFileNamed: file)].

^ Forms at: file.	