as yet unclassified
testEntries
 | files nodes |

nodes := OrderedCollection new.
files := MontageTimeline imagesInDirectory: '/home/dto/crisis/city'.
files do: [:file | | node | 
	node := MontageTimelineNode new.
	node duration: 8; transform: '[montage-pan-down :speed 1.0]'; imageFile: file.
	nodes add: node.].	
^ nodes copyFrom: 1 to: 5