as yet unclassified
imagesInDirectory: dirname
	| results dir |
	results := OrderedCollection new.
	dir := (FileDirectory default directoryNamed: dirname).
	dir entries
		do: [:entry | ('*.png' match: entry name)
				ifTrue: [results add: (dir fullNameFor: entry name)]].
	^ results