as yet unclassified
thumbnailForFile: file
| morph form |
Thumbnails ifNil: [Thumbnails := Dictionary new].
(Thumbnails at: file ifAbsent: [Thumbnails at: file put: ((self formForFile: file) scaledIntoFormOfSize: self thumbnailSize)]).
form := Thumbnails at: file.
morph := MontageImageMorph new.
morph image: form; imageFile: file.
morph setNameTo: file.
^ morph