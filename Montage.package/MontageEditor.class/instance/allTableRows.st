as yet unclassified
allTableRows
| cells end row rows |
cells := self allTableCells asOrderedCollection.
row := OrderedCollection new.
rows := OrderedCollection new. 
end := cells size.

(1 to: ((cells size) // 3)) do: 
	[ :node |  row := OrderedCollection with: (cells at: 1) with: (cells at: 2) with: (cells at: 3).
			rows addLast: row.
			(1 to: 3 do: [ :n | cells removeFirst ])].

^ rows