as yet unclassified
open
	| model table handler viewport |
	self openUi.
	self startStepping.
	MontageTimeline clearImageCaches.
	MontageEditor resizeSomeWidgetText: 12.
	model := MontageTimelineModel new.
	table := self ui timelineTable.
	viewport := table viewport.
	table addColumn: 'Duration'	
		for: ((UiItemViewColumnSpec newFrom: {#text -> #duration})
				textConverter: [:integer | integer asString asHtmlText]);
		
		addColumn: 'Transform'
		for: ((UiItemViewColumnSpec newFrom: {#text -> #transform})
				textConverter: [:string | string asHtmlText]);
		
		addColumn: 'Image' for: ((UiItemViewColumnSpec newFrom: {#morph -> #thumbnailMorph})
				morphConverter: [:morph | 
					morph on: #mouseDown send: #updateImagePreviewArea to: morph.					
					morph
					]);
		 model: model;
		 setHAlignmentForColumn: 2 to: #left;
		 setHAlignmentForColumn: 3 to: #right;
		 topLeft: 50 @ 50;
		 extent: 600 @ 300;
		 addVerticalHeader;
		 addHorizontalHeader.
		 handler := (self ui timelineTable viewport eventHandler).
		 handler mouseDownPriority: 50. 
		
		" viewport disconnectSignal: #mouseClicked from: handler;
				disconnectSignal: #mousePressed from: handler; 
				disconnectSignal: #mouseReleased from: handler;
				disconnectSignal: #mouseDoubleClicked from: handler.
		"		
	self openInWindowLabeled: 'Montage Editor'.
