as yet unclassified
makeTimelineLisp
| rows lisp strings | 
lisp := '(montage-process-timeline ''('.
rows := self allTableRows.
strings := rows collect: [ :row | '(', (row at: 1), ' ', (row at: 2), ' "', (row at: 3), '")', (String with: Character cr).].
strings do: [ :row | lisp := lisp, row].
^ lisp, ')))  (unless montage-inhibit-render (montage-run-script)) '
