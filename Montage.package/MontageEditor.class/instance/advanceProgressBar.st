as yet unclassified
advanceProgressBar
	"Tick the progress bar forward by 5 percent, wrapping around to zero at 100."
	| bar |
	bar := self ui progressBar.
	bar currentValue: 5 + bar currentValue \\ 100;
		 text: 'Processing...'