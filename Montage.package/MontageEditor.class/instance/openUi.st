as yet unclassified
openUi

	self ui setupUi: self.
	self ui progressBar
		currentValue: 0;
		text: 'Status: Idle'.

	self startStepping.	

	self ui renderButton
		connect: (self ui renderButton)
		signal: #pressed
		to: self
		selector: #render.
	
	self ui renderAllButton
		connect: (self ui renderAllButton)
		signal: #pressed
		to: self
		selector: #renderAll.
	
	self ui previewButton
		connect: (self ui previewButton)
		signal: #pressed
		to: self
		selector: #preview.

	self ui previewAllButton
		connect: (self ui previewAllButton)
		signal: #pressed
		to: self
		selector: #previewAll.
		
	self ui openProjectButton
		connect: (self ui openProjectButton)
		signal: #pressed
		to: self
		selector: #openProject.
		
	"self ui closeProjectButton
		connect: (self ui closeProjectButton)
		signal: #pressed
		to: self
		selector: #closeProject.
	"	
	self ui addButton
		connect: (self ui addButton)
		signal: #pressed
		to: self
		selector: #addNode.
		
	self ui removeButton
		connect: (self ui removeButton)
		signal: #pressed
		to: self
		selector: #removeNode.
		
	self ui moveUpButton
		connect: (self ui moveUpButton)
		signal: #pressed
		to: self
		selector: #moveUp.
		
	self ui moveDownButton
		connect: (self ui moveDownButton)
		signal: #pressed
		to: self
		selector: #moveDown.
		
	self ui undoButton
		connect: (self ui undoButton)
		signal: #pressed
		to: self
		selector: #undo.
		
	self ui redoButton
		connect: (self ui redoButton)
		signal: #pressed
		to: self
		selector: #redo.
		
	self ui cutButton
		connect: (self ui cutButton)
		signal: #pressed
		to: self
		selector: #cutRows.
			
	self ui copyButton
		connect: (self ui copyButton)
		signal: #pressed
		to: self
		selector: #copyRows.

	self ui pasteButton
		connect: (self ui pasteButton)
		signal: #pressed
		to: self
		selector: #pasteRows.

	self ui cancelButton
		connect: (self ui cancelButton)
		signal: #pressed
		to: self
		selector: #cancelRender.

	self ui clearCacheButton
		connect: (self ui clearCacheButton)
		signal: #pressed
		to: self
		selector: #clearCache.
			
		
		
		.


	
	
	
	
