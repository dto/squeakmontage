as yet unclassified
makeVariableLisp
| dict lisp |
lisp := '(montage-apply-variables '.
dict := Dictionary new.
{ 'montage-project-directory' -> ('"', (self ui projectPathEntry theText asString), '"').
'montage-bpm' -> (self ui beatsPerMinuteEntry theText asNumber).
'montage-fps' -> (self ui framesPerSecondEntry theText asNumber).
'montage-audio-file' -> ('"', (self ui audioFileEntry theText asString), '"').
'montage-video-width' -> (self ui videoWidthEntry theText asString asInteger).
'montage-video-height' -> (self ui videoHeightEntry theText asString asInteger).
'montage-upscale-factor' -> (self ui upscaleFactorEntry theText asString asInteger).
'montage-inhibit-render' -> ((self ui generateScriptOnly checked)
								ifTrue: ['t'] ifFalse: ['nil'] )}
	do: [ :each | dict add: each]. 
dict keysAndValuesDo: [ :key :value | lisp := (lisp, ' ', key, ' ', value, ' ')].
^ lisp, ')'.
		
