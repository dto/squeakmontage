as yet unclassified
allTableCells
| cells cell |
cell := nil.
cells := ((((self ui timelineTable) submorphs at: 6) submorphs at: 1) submorphs) asOrderedCollection.
cells removeFirst.
^ cells collect: [ :each | 
					cell := (each submorphs at: 1).
					(cell isKindOf: ImageMorph) 
						ifTrue: [ cell knownName]
						 ifFalse: [cell contents string]].
						