as yet unclassified
resizeSomeWidgetText: points
	UiTextEdit allInstances do: [:each | each textMorph fontName: '' pointSize: points].
	UiLineEdit allInstances do: [:each | each textMorph fontName: '' pointSize: points].
	UiLabel allInstances do: [:each | each textMorph fontName: '' pointSize: points].
"	UiViewHeaderItem allInstances do: [:each || a b | 
		a := each submorphs ifNotEmpty: [each submorphs at: 1].
		b := (a isKindOf: Array) ifTrue: [^ self]
								ifFalse: [a submorphs ifNotEmpty: [a submorphs at: 1]].
		(b isKindOf: TextMorph) ifTrue: [b fontName: '' pointSize: points]].
"
