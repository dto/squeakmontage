as yet unclassified
reportCompleted
	"Tell the user that processing finished successfully."
	| timestring stream |
	timestring := String new.
	stream := WriteStream on: timestring.
	stream nextPutAll: 'Total: '.
	startTime ifNotNil: [(startTime to: Time now) asDuration printOn: stream.
						self tellUser: ('Completed {1} tasks in {2}' format: {1. stream contents})].
	consoleInstance ui progressBar currentValue: 0;
		text: 'Completed.'.
	(SampledSound playSoundNamed: 'click'). 