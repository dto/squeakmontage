as yet unclassified
createRunner 
	| runner | 
	runner := MontageRunner new.
	runner sourcePath: (MontageEditor newestInstance ui sourcePathEntry theText asString).
	runner projectPath: (MontageEditor newestInstance ui projectPathEntry theText asString).
	runner lispExecutable: (MontageEditor newestInstance ui lispExecutableEntry theText asString).
	^ runner